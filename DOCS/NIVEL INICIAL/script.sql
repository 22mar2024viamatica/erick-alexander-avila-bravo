-- Crear la base de datos
CREATE DATABASE p_telconet_db;

-- Crear la tabla Usuario
CREATE TABLE Users (
    idUser SERIAL PRIMARY KEY,
    fullname VARCHAR(100) NOT NULL,
    username VARCHAR(50) UNIQUE NOT NULL,
    mail VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(150) NOT NULL,
    state VARCHAR(20) DEFAULT 'Activo' NOT NULL
);

CREATE TABLE user_roles (
    id_user INT REFERENCES Users(idUser),
    id_role INT REFERENCES Roles(idRole),
    PRIMARY KEY (id_user, id_role)
);

-- Crear la tabla Producto
CREATE TABLE Products (
    idProduct SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL CHECK (LENGTH(name) >= 1 AND LENGTH(name) <= 50),
    amount INT NOT NULL,
    price FLOAT NOT NULL,
    image VARCHAR(255),
    state VARCHAR(20) DEFAULT 'Activo' NOT NULL
);
