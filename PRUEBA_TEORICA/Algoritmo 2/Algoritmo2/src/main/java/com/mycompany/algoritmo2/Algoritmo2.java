/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.algoritmo2;

import java.util.Scanner;

/**
 *
 * @author erik-avila
 */
public class Algoritmo2 {

    public static void main(String[] args) {
        
        /*
         * Solicita el ingreso de un numero por consola
         */
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número/>> ");
        int num = scanner.nextInt();
        
        /**
         * Verifica si el numero esta dentro del rango valido 
         */
        if (num < Integer.MIN_VALUE || num > Integer.MAX_VALUE) {
            System.out.println("El número está fuera del rango permitido.");
        }
        
        /**
         * Muestra el resultado
         */
        System.out.println("¿Es un palíndromo?");
        System.out.print(isPalindrome(num));

        scanner.close();
    }

    /**
     * Función para verificar si un número es un palíndromo
     */
    public static boolean isPalindrome(int x) {
        // Verificar si el número es negativo o termina en 0, en cuyo caso no es un palíndromo
        if (x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        }

        int revertedNumber = 0;
        
        // Invertir la mitad del número
        while (x > revertedNumber) {
            revertedNumber = revertedNumber * 10 + x % 10;
            x /= 10;
        }
        
        // Si la longitud del número original es impar, necesitamos eliminar el dígito del medio
        return x == revertedNumber || x == revertedNumber / 10;
    }

}
