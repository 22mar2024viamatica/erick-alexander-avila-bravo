/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.algoritmo1;

import java.util.Scanner;

/**
 *
 * @author erik-avila
 */
public class Algoritmo1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        /**
         * *
         * Declaracion de variables para datos de entrada
         */
        System.out.println("Se debe ingresar las horas en formato militar:");
        System.out.print("Ingrese la primera hora/>> ");
        String time1 = scanner.next();
        System.out.print("Ingrese la segunda hora/>> ");
        String time2 = scanner.next();

        /***
         * Obtiene las horas y minutos por separado de cada tiempo
         */
        int[] hourMinute1 = getHourMinute(time1);
        int[] hourMinute2 = getHourMinute(time2);
        
        /**
         *Calcula los minutos totales 
         */
        int minutes1 = hourMinute1[0] * 60 + hourMinute1[1];
        int minutes2 = hourMinute2[0] * 60 + hourMinute2[1];
        
        int minuteDifference = Math.abs(minutes2 - minutes1); // Calcula los minutos totales dados en los dos tiempos

        /***
         * Si el segundo tiempo es anterior al primero se ajustar la diferencia
         */
        if (minutes2 < minutes1) {
            minuteDifference = 1440 - minuteDifference;
        }
        
        // Se calculan las horas y minutos a partir de la diferencia total en minutos. 
        int hours = minuteDifference / 60;
        int remainingMinutes = minuteDifference % 60;
        
        /***
         * Se muestra el resultado
         */
        System.out.println("Response: " + hours + " hours " + remainingMinutes + " minutes"); // Muestra el resultado
        scanner.close();
    }

    /**
     * Funcion para obtener la hora y minuto por separado
     */
    public static int[] getHourMinute(String time) {
        int[] hourMinute = new int[2];
        hourMinute[0] = Integer.parseInt(time.substring(0, 2));
        hourMinute[1] = Integer.parseInt(time.substring(2));
        return hourMinute;
    }
}
