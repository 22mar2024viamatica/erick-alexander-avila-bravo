/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.interfaces;

import ec.telconet.msCompPruebaErickAvila.models.Product;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author erik-avila
 */
public interface ProductInterface {
    
    public Iterable<Product> findAll();

    public Optional<Product> findById(Long id);

    public Product save(Product product);

    public Product edit(Long id, Map<String, String> product);

    public void delete(Long id);
    
    // More functions
    public Optional<Product> findByProductName(String name);
    
    public Product uploadImage(Long id, Product image);
    
}
