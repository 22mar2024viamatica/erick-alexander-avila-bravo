/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.controllers;

import ec.telconet.msCompPruebaErickAvila.DTO.AuthDTO;
import ec.telconet.msCompPruebaErickAvila.models.User;
import ec.telconet.msCompPruebaErickAvila.services.UserService;
import jakarta.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author erik-avila
 */
@Controller
@RequestMapping("/api/auth")
public class AuthController {
    
    @Autowired
    private UserService UserService;
    
    private Map<String, Object> response = new HashMap<>();
    
    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody AuthDTO data) {
        String user = data.getUser();
        String password = data.getPassword();
        User userLogin = new User();

        // Verification if exits user
        if (!UserService.findByUsername(user).isPresent()) {
            response.clear();
            response.put("message", "User not found.");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }

        // Login verification by username or email
        if (UserService.findByUsername(user).isPresent()) {
            Optional<User> optionalUser = UserService.findByUsername(user);
            userLogin = optionalUser.get();
        } else {
            Optional<User> optionalMail = UserService.findByMail(user);
            userLogin = optionalMail.get();
        }

        
        // Access Password Validation
        if (!userLogin.getPassword().equals(password)) {
            response.clear();
            response.put("message", "Incorrect password.");
            return ResponseEntity.badRequest().body(response);
        }

        UserService.save(userLogin);

        // Login user data
        List<Object> roles = UserService.findRolesByIdUser(userLogin.getIdUser());
        response.clear();
        response.put("message", "Login successfull");
        response.put("idUser", userLogin.getIdUser());
        response.put("username", userLogin.getUsername());
        response.put("roles", roles);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    
    @PostMapping("/logout")
    public ResponseEntity<?> logout(@RequestBody Map<String, Long> data) {
        Long user = data.get("idUser");

        Optional<User> userLogout = UserService.findById(user);

        if (!userLogout.isPresent()) {
            response.clear();
            response.put("message", "User not found.");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }

        response.clear();
        response.put("message", "Logout successfull.");
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    
}
