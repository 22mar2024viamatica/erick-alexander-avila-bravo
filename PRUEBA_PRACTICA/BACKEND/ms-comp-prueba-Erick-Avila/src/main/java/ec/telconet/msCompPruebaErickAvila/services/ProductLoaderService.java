/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec.telconet.msCompPruebaErickAvila.DTO.ProductDTO;
import ec.telconet.msCompPruebaErickAvila.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author erik-avila
 */
@Service
public class ProductLoaderService {

    @Autowired
    private ProductService productService;
    private RestTemplate restTemplate;

    public ProductLoaderService(ProductService productService, RestTemplate restTemplate) {
        this.productService = productService;
        this.restTemplate = restTemplate;
    }

    public void LoadProductsEmpoint(String endpoint) {
        ResponseEntity<String> response = this.restTemplate.getForEntity(endpoint, String.class);

        try {
            // Parsea la respuesta usando Jackson
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode products = root.path("products");
            
           if (products.isArray()) {
                for (JsonNode productNode : products) {
                   ProductDTO prod = mapper.treeToValue(productNode, ProductDTO.class);

                    // Comprueba si el producto ya existe
                    if (productService.findByProductName(prod.getTitle()).isEmpty()) {
                        // Crea y guarda el producto en la base de datos
                        Product product = new Product();
                        product.setName(prod.getTitle());
                        product.setAmount(prod.getStock());
                        product.setPrice((float) prod.getPrice());
                        product.setState("Activo");

                        productService.save(product);
                    }
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    
    
    
   

        

}
