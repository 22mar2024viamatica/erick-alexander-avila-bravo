/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.controllers;

import ec.telconet.msCompPruebaErickAvila.models.Product;
import ec.telconet.msCompPruebaErickAvila.services.ProductService;
import jakarta.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author erik-avila
 */
@Controller
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductService service;

    private Map<String, Object> response = new HashMap<>();

    @GetMapping("/products")
    public ResponseEntity<?> findAll() {
        response.clear();
        response.put("products", service.findAll());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        Optional<Product> product = service.findById(id);
        if (!product.isPresent()) {
            response.clear();
            response.put("message", "Product not found.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        response.clear();
        response.put("product", service.findById(id));
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping("/product/save")
    public ResponseEntity<?> save(@Valid @RequestBody Product data) {
        try {
            Optional<Product> product = service.findByProductName(data.getName());
            if (product.isPresent()) {
                response.clear();
                response.put("message", "This product is already registered.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            Product saved = service.save(data);
            response.clear();
            response.put("message", "Product registered successfully");
            response.put("product", saved);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PutMapping("/product/edit/{id}")
    public ResponseEntity<?> edit(@PathVariable Long id, @RequestBody Map<String, String> data) {
        try {
            Optional<Product> product = service.findById(id);
            if (!product.isPresent()) {
                response.clear();
                response.put("message", "Product not found.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            Product edited = service.edit(id, data);
            response.clear();
            response.put("message", "Product edited successfully");
            response.put("product", edited);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @DeleteMapping("/product/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Optional<Product> productValidate = service.findById(id);

        if (!productValidate.isPresent()) {
            response.clear();
            response.put("message", "Product not found.");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }

        service.delete(id);
        response.clear();
        response.put("message", "Product deleted successfully.");
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    // More fuctions
    @PutMapping("/product/upload_image/{id}")
    public ResponseEntity<?> uploadImage(@PathVariable Long id, @RequestParam("image") MultipartFile data) {
        try {
            Optional<Product> product = service.findById(id);
            if (!product.isPresent()) {
                response.clear();
                response.put("message", "Product not found.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            String directory = "./Assets/img/";
            
            File directoryFile = new File(directory);
            if (!directoryFile.exists()) { // Crear el directorio si no existe
                directoryFile.mkdirs();
            }
            
            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            String formattedDateTime = date.format(formatter);
            String image = formattedDateTime + "_" + data.getOriginalFilename().replaceAll("[-]", "");

            // Generar un Path para el destino
            Path destinationPath = Paths.get(directory, image);
            Files.copy(data.getInputStream(), destinationPath, StandardCopyOption.REPLACE_EXISTING);

            product.get().setImage(image);

            Product editedImage = service.uploadImage(id, product.get());
            response.clear();
            response.put("message", "Image upload successfully");
            response.put("product", editedImage);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping("/product/image/{id}")
    public ResponseEntity<?> obtenerImagen(@PathVariable Long id) {
        Optional<Product> product = service.findById(id);
        if (!product.isPresent()) {
            response.clear();
            response.put("message", "Product not found.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        product.get().getName();
        String directory = "./Assets/img/";
        String image = product.get().getImage();

        try {
            Path path = Paths.get(directory, image);
            byte[] imageByte = Files.readAllBytes(path);
            return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.IMAGE_JPEG).body(imageByte);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

}