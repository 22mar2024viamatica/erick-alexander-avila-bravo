/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.Set;

/**
 *
 * @author erik-avila
 */
@Entity
@Table(name = "Users")
public class User {

    /**
     * Se declaran los campos para el modelo productos de acuerdo a los
     * requerimientos solicitados
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idUser;
    
    @NotNull(message = "This filed is required.")
    @Column(length = 100)
    private String fullname;
    
    @NotNull(message = "This filed is required.")
    @Column(length = 50, unique = true)
    private String username;

    @NotNull(message = "This filed is required.")
    @Email(message = "El correo debe tener un formato válido")
    @Column(length = 100, unique = true)
    private String mail;

    @NotNull(message = "This filed is required.")
    @Size(min = 8, message = "Password must be at least 8 characters.")
    @Column(length = 150)
    private String password;

    @Column(length = 20)
    private String state = "Activo";

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles", joinColumns = {
        @JoinColumn(name = "id_user")}, inverseJoinColumns = {
        @JoinColumn(name = "id_role")}
    )
    private Set<Role> roles;

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

}
