/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.services;

import ec.telconet.msCompPruebaErickAvila.interfaces.ProductInterface;
import ec.telconet.msCompPruebaErickAvila.models.Product;
import ec.telconet.msCompPruebaErickAvila.repositories.ProductRepository;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author erik-avila
 */
@Service
public class ProductService implements ProductInterface {
    
    @Autowired
    private ProductRepository repository;
    
    @Override
    @Transactional(readOnly = true)
    public Iterable<Product> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Product> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    @Transactional
    public Product save(Product product) {
        return repository.save(product);
    }

    @Override
    @Transactional
    public Product edit(Long id, Map<String, String> product) {
        Optional<Product> editedProduct = repository.findById(id);
        editedProduct.get().setName(product.get("name"));
        editedProduct.get().setAmount(Integer.parseInt(product.get("amount")));
        editedProduct.get().setPrice(Float.valueOf(product.get("price")));
        
        return repository.save(editedProduct.get());
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.deleteById(id);
    }
    
    // More functions
    @Transactional(readOnly = true)
    public Optional<Product> findByProductName(String name) {
        return repository.findByProductName(name);
    }
    
    @Override
    @Transactional
    public Product uploadImage(Long id, Product image) {
        Optional<Product> upload = repository.findById(id);
        upload.get().setImage(image.getImage());
        return repository.save(upload.get());
    }
       
}