/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 *
 * @author erik-avila
 */
@Entity
@Table(name = "Products")
public class Product {
    
    /**
     * Se declaran los campos para el modelo productos de acuerdo a los
     * requerimientos solicitados
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idProduct;

    @Column(length = 50, nullable = false)
    @NotBlank(message = "This field is required.")
    @Size(min = 1, max = 50, message = "Name must be 50 characters long")
    private String name;
    
    @Column(nullable = false)
    @NotNull(message = "This field is required.")
    private int amount;

    @Column(nullable = false)
    @NotNull(message = "This field is required.")
    private float price;

    private String image;

    @Column(length = 20)
    private String state = "Activo";

    public Long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Long idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
}
