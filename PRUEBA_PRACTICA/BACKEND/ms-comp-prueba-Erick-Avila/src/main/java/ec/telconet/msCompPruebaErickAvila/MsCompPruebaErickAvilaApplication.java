package ec.telconet.msCompPruebaErickAvila;

import ec.telconet.msCompPruebaErickAvila.services.ProductLoaderService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCompPruebaErickAvilaApplication {
    @Autowired
    private ProductLoaderService service;
    public static void main(String[] args) {
        SpringApplication.run(MsCompPruebaErickAvilaApplication.class, args);   
    }
    
    @PostConstruct
    public void LoadProductsEmpoint() { // Se cargan los productos registrados en el siguiente endpoint
        service.LoadProductsEmpoint("https://dummyjson.com/products");
    }
}
