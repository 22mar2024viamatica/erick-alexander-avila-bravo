/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.interfaces;

import ec.telconet.msCompPruebaErickAvila.models.Role;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author erik-avila
 */
public interface RoleInterface {
    
    public List<Role> findAll();

    public Optional<Role> findById(Long id);

    public Role save(Role role);

    public Role edit(Long id, Role role);

    public void delete(Long id);
    
}
