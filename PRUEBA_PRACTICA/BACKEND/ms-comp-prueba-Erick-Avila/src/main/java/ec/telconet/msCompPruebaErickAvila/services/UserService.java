/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.services;

import ec.telconet.msCompPruebaErickAvila.interfaces.UserInterface;
import ec.telconet.msCompPruebaErickAvila.models.User;
import ec.telconet.msCompPruebaErickAvila.repositories.UserRepository;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author erik-avila
 */
@Service
public class UserService implements UserInterface {

    @Autowired
    private UserRepository repository; // Se inyecta el repositorio de usuarios

    @Override
    @Transactional(readOnly = true)
    public Iterable<User> findAll() { // Se extrae todos los usuarios
        return repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findById(Long id) { // Se extrae un solo usuario por el id
        return repository.findById(id);
    }

    @Override
    @Transactional()
    public User save(User user) { // Se guarga un usuario
        return repository.save(user);
    }

    @Override
    @Transactional()
    public User edit(Long id, Map<String, String> user) { // Se edita la informacion necesaria de un usuario por el id
        User userEdited = repository.findById(id).orElse(null);
        userEdited.setFullname(user.get("fullname"));
        userEdited.setUsername(user.get("username"));
        userEdited.setMail(user.get("mail"));
        
        return repository.save(userEdited);
    }

    @Override
    @Transactional()
    public void delete(Long id) { // Se elimina un usuario por el id
        repository.deleteById(id);
    }
    
    // More functions

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findByUsername(String username) { // Se estrae el nombre de usuario de un usuario por id
        return repository.findByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object> findRolesByIdUser(Long id) { // Se extrae los roles de un usuario por id
        return repository.findRolesByIdUser(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Optional<User> findByMail(String mail) {
        return repository.findByMail(mail);
    }

    @Override
    @Transactional
    public User stateChange(Long id, Map<String, String> state) {
        Optional<User> change = repository.findById(id);
        change.get().setState(state.get("state"));
        return repository.save(change.get());
    }
}
