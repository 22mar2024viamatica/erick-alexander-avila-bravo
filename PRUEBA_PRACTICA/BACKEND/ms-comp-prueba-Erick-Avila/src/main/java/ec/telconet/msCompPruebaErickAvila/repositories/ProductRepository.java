/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.repositories;

import ec.telconet.msCompPruebaErickAvila.models.Product;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author erik-avila
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
    
    // Consulta para verificar si el producto ya existe en la base de datos
    @Query(value = "SELECT * FROM products WHERE name = :name", nativeQuery = true)
    public Optional<Product> findByProductName(String name);
}
