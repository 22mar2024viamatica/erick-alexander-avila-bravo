/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ec.telconet.msCompPruebaErickAvila.repositories;

import ec.telconet.msCompPruebaErickAvila.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author erik-avila
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    
}