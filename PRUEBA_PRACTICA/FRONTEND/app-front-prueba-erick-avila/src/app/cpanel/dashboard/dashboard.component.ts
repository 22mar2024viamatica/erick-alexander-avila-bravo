import { Component, OnInit } from '@angular/core';

import { AppService } from 'src/app/services/app.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UsersService } from 'src/app/services/cpanel/users/users.service';
import { ProductsService } from 'src/app/services/cpanel/products/products.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  userData: any = localStorage.getItem('userData');
  idUser: number = 0;
  username: string = '';
  role: string | null = localStorage.getItem('rol');

  users: any[] = [];
  products: any[] = [];


  constructor(
    private AppService: AppService,
    private AuthService: AuthService,
    private UsersService: UsersService,
    private ProductsService: ProductsService
  ) {
    this.AuthService.loginFalse();
    this.AppService.sidebar('dashboard-item');
  }

  ngOnInit(): void {
    if (this.userData !== null) {
      let userdata = JSON.parse(this.userData);
      this.idUser = userdata.user.idUser;
      this.username = userdata.user.username;

      this.UsersService.getUsers().subscribe(
        response => {
          // Ordenar los usuarios por idUser en orden descendente
          const sortedUsers = response.users.sort((a: any, b: any) => b.idUser - a.idUser);

          // Obtener los últimos 5 usuarios
          this.users = sortedUsers.slice(0, 5);
        }
      );

      this.ProductsService.getProducts().subscribe(
        response => {
          // Ordenar los usuarios por idUser en orden descendente
          const sortedPeoduct = response.products.sort((a: any, b: any) => b.idProduct - a.idProduct);

          // Obtener los últimos 5 usuarios
          this.products = sortedPeoduct.slice(0, 5);
        }
      );
    }
  }
}
