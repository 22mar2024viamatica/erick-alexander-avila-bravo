import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { AuthService } from 'src/app/services/auth/auth.service';
import { AppService } from 'src/app/services/app.service';
import { ProductsService } from 'src/app/services/cpanel/products/products.service';

interface product {
  name: string;
  amount: string;
  price: number;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  @ViewChild('ModalNewProduct') ModalNewProduct?: ModalDirective;
  @ViewChild('ModalEditProduct') ModalEditProduct?: ModalDirective;
  @ViewChild('ModalSeeProduct') ModalSeeProduct?: ModalDirective;
  @ViewChild('ModalUploadImage') ModalUploadImage?: ModalDirective;
  @ViewChild('ModalSendMail') ModalSendMail?: ModalDirective;


  idRole: string | null = localStorage.getItem('rolId');
  role: string | null = localStorage.getItem('rol');

  /**
   * Propiedades de un producto
   */
  products: any[] = [];
  idProduct: number = 0;
  name: string = '';
  amount: string = '';
  price: number = 0;
  state: string = '';
  image: any = null;

  newProduct: product = { // Interface para el registro de un nuevo producto
    name: '',
    amount: '',
    price: 0
  }

  /** Sendmail */
  to: string = '';
  subject: string = '';
  message: string = '';

  /** Filtros */
  productsFilter: any[] = [];
  nameFilter: string = '';
  valueFilter: string = '';
  creationDateFilter: string = '';
  saleDateFilter: string = '';
  stateFilter: string = '';


  /** Paginado */
  currentPage = 1;
  recordPerPage = 5;

  constructor(
    private AppService: AppService, private AuthService: AuthService,
    private toastr: ToastrService, private ProductsService: ProductsService
  ) {
    this.AuthService.loginFalse();
    this.AppService.sidebar('products-item');
  }

  ngOnInit(): void {
    this.ProductsService.getProducts().subscribe( // Mostrar todos los productos en la tabla
      response => {
        this.products = response.products;
        this.productsFilter = this.products;
      }
    );
  }

  /**
   * Modals
   */
  openModalNewProduct() { // Formulario para registrar un producto
    this.ModalNewProduct?.show();
  }

  openModalSeeProduct(id: number) { // Ventana para visualizar toda la informacion de un producto
    this.ProductsService.getProduct(id).subscribe(
      response => {
        this.idProduct = response.product.idProduct;
        this.name = response.product.name;
        this.amount = response.product.amount;
        this.price = response.product.price;
        this.state = response.product.state;
      }
    );

    this.image = null;
    this.ProductsService.getImage(id).subscribe(
      (image: any) => {
        const reader = new FileReader();
        reader.readAsDataURL(image);
        reader.onloadend = () => {
          this.image = reader.result;
        }
      }
    );

    this.ModalSeeProduct?.show();
  }

  openModalEditProduct(id: number) { // Formulario para editar un producto
    this.ProductsService.getProduct(id).subscribe(
      response => {
        this.idProduct = response.product.idProduct;
        this.name = response.product.name;
        this.amount = response.product.amount;
        this.price = response.product.price;
      }
    );
    this.ModalEditProduct?.show();
  }

  openModalUploadImage(id: number) { // Ventana para subir una imagen
    this.idProduct = id;
    this.image = null;
    this.ProductsService.getImage(id).subscribe(
      (image: any) => {
        const reader = new FileReader();
        reader.readAsDataURL(image);
        reader.onloadend = () => {
          this.image = reader.result;
        }
      }
    );

    this.ModalUploadImage?.show();
  }

  /**
   * Consumiendo los servicios de la api para el CRUD
   */
  save() { // Registar un nuevo producto
    let data = {
      name: this.newProduct.name,
      amount: this.newProduct.amount,
      price: this.newProduct.price
    }

    this.ProductsService.save(data).subscribe(
      response => {
        if (response.product) {
          this.toastr.success(response.message, '¡Listo!', { closeButton: true });
          this.ModalNewProduct?.hide();
          this.resetFormNewProduct();
          this.ngOnInit();
        }
      },
      error => {
        this.toastr.warning(error, '¡Atención!', { closeButton: true });
      }
    );

  }

  edit(id: number) { // Editar un producto
    let data = {
      name: this.name,
      amount: this.amount,
      price: this.price
    }

    this.ProductsService.edit(id, data).subscribe(
      response => {
        if (response.product) {
          this.toastr.success(response.message, '¡Listo!', { closeButton: true });
          this.ModalEditProduct?.hide();
          this.ngOnInit();
        }
      },
      error => {
        this.toastr.warning(error, '¡Atención!', { closeButton: true });
      }
    );
  }

  delete(id: number) { // Eliminar un producto
    Swal.fire({
      icon: 'warning',
      title: '<strong>¿Esta seguro que desea eliminar este registro?</strong>',
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.ProductsService.delete(id).subscribe(
          response => {
            if (response.message) {
              this.toastr.success(response.message, '¡Listo!', { closeButton: true });
              this.ngOnInit();
            }
          },
          error => {
            this.toastr.warning(error, '¡Atención!', { closeButton: true });
          }
        );
      }
    });
  }

  /**
   * More functions
   */
  uploadImage(id: number) { // Subir una imagen para el producto
    let data = document.querySelector('#uploadImage');
    this.ProductsService.uploadImage(id, data).subscribe(
      response => {
        if (response.product) {
          this.toastr.success(response.message, '¡Listo!', { closeButton: true });
          this.ModalUploadImage?.hide();
          this.ngOnInit();
        }
      },
      error => {
        this.toastr.warning(error, '¡Atención!', { closeButton: true });
      }
    );
  }

  statusChange(id: number, state: string) { // Cambiar el estado del producto
    let data = {
      state: state
    }
    this.ProductsService.statusChange(id, data).subscribe(
      response => {
        if (response.product) {
          this.toastr.success(response.message, '¡Listo!', { closeButton: true });
          this.ngOnInit();
        }
      },
      error => {
        this.toastr.warning(error, '¡Atención!', { closeButton: true });
      }
    );
  }

  /**
   * Paginado
   */
  countRangeRegister(): string {
    const startIndex = (this.currentPage - 1) * this.recordPerPage + 1;
    const endIndex = Math.min(startIndex + this.recordPerPage - 1, this.productsFilter.length);
    let msg;
    endIndex === 0 ? msg = 'No hay registros.' : msg = `Mostrando registros del ${startIndex} al ${endIndex}`;
    return msg;
  }

  /**
   * Resetear formularios
   */
  resetFormNewProduct() { // Resetear el formulario para registrar un producto
    this.newProduct.name = '';
    this.newProduct.amount = '';
    this.newProduct.price = 0;
  }
}