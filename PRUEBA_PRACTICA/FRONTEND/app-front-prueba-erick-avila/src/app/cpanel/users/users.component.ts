import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';


import { AuthService } from 'src/app/services/auth/auth.service';
import { AppService } from 'src/app/services/app.service';
import { UsersService } from 'src/app/services/cpanel/users/users.service';

interface User {
  fullname: string;
  username: string;
  mail: string;
  password: string;
}


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @ViewChild('ModalBulkLoad') ModalBulkLoad?: ModalDirective;
  @ViewChild('ModalSaveUser') ModalSaveUser?: ModalDirective;
  @ViewChild('ModalEditUser') ModalEditUser?: ModalDirective;
  @ViewChild('usersTable') usersTable!: ElementRef;

  idRole: string | null = localStorage.getItem('rolId');
  role: string | null = localStorage.getItem('rol');

  users: any[] = [];
  usersFilter: any[] = [];
  idUser: number = 0;
  fullname: string = '';
  username: string = '';
  mail: string = '';
  status: string = '1';

  newUser: User = { // Interface para el registro de un nuevo usuario
    fullname: '',
    username: '',
    mail: '',
    password: '',
  }

  //Bulk Load of users
  data: string[][] = [];
  dataTable: string[][] = [];
  showTable: boolean = false;

  search: string = '';

  constructor(
    private AppService: AppService, private AuthService: AuthService,
    private UsersService: UsersService, private toastr: ToastrService
  ) {
    this.AuthService.loginFalse();
    this.AppService.sidebar('users-item');
  }

  ngOnInit(): void {
    this.UsersService.getUsers().subscribe(
      response => {
        this.users = response.users.filter((user: any) => user.state === 'Activo');
        this.usersFilter = this.users;
      },
      error => {
        console.error('Error al obtener datos de la API:', error);
      }
    );
  }

  /**
   * Modals o ventanas emergentes para crear y editar
   */
  openModalBulkLoad() {
    this.ModalBulkLoad?.show();
  }

  openModalSaveUser() {
    this.ModalSaveUser?.show();
  }

  openModalEditUser(id: number) {
    this.UsersService.getUser(id).subscribe(
      response => {
        this.idUser = response.user.idUser;
        this.fullname = response.user.fullname;
        this.username = response.user.username;
        this.mail = response.user.mail;
      }
    );
    this.ModalEditUser?.show();
  }

  /**
   * Consumiendo los servicios de la api
   */
  save() { // Registar un nuevo producto
    let data = {
      fullname: this.newUser.fullname,
      username: this.newUser.username,
      mail: this.newUser.mail,
      password: this.newUser.password,
      roles: [{ idRole: '2', role: 'Usuario' }]
    }

    this.UsersService.save(data).subscribe(
      response => {
        if (response.user) {
          this.toastr.success(response.message, '¡Listo!', { closeButton: true });
          this.ModalSaveUser?.hide();
          this.ngOnInit();
        }
      },
      error => {
        this.toastr.warning(error, '¡Atención!', { closeButton: true });
      }
    );

  }

  edit(id: number) {
    let data = {
      idUser: this.idUser,
      fullname: this.fullname,
      username: this.username,
      mail: this.mail
    }

    this.UsersService.edit(id, data).subscribe(
      response => {
        if (response) {
          this.toastr.success(response.message, '¡Listo!', { closeButton: true });
          this.ngOnInit();
          this.ModalEditUser?.hide();
        }
      },
      error => {
        this.toastr.warning(error, '¡Atención!', { closeButton: true });
      }
    );
  }

  statusChange(id: number, state: string) { // Cambiar el estado del usuario a inactivo que equivale a eliminado
    Swal.fire({
      icon: 'warning',
      title: '<strong>¿Esta seguro que desea eliminar este registro?</strong>',
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        let data = {
          state: state
        }
        this.UsersService.statusChange(id, data).subscribe(
          response => {
            if (response.user) {
              this.toastr.success(response.message, '¡Listo!', { closeButton: true });
              this.ngOnInit();
            }
          },
          error => {
            this.toastr.warning(error, '¡Atención!', { closeButton: true });
          }
        );
      }
    });
  }


  /**
   * More functions
   */
  onFileSelected(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (e: any) => {
      const content = e.target.result;
      const lines = content.split('\n');

      this.data = [];

      for (const line of lines) {
        if (line.trim() === '') {
          continue;
        }

        const field = line.split(',');
        const user: any = {
          fullname: field[0],
          username: field[1],
          mail: field[2],
          password: field[3],
          roles: [{
            idRole: '2',
            role: 'Usuario'
          }]
        };
        this.data.push(user);
        this.dataTable.push(field);
      }

      this.showTable = true;
    };

    reader.readAsText(file);
  }

  bulkLoad() {
    this.UsersService.bulkLoad(JSON.stringify(this.data)).subscribe(
      response => {
        if (response.message) {
          this.toastr.success(response.message, '¡Listo!', { closeButton: true });
          this.ModalBulkLoad?.hide();
          this.ngOnInit();
        }
      },
      error => {
        this.toastr.warning(error, '¡Atención!', { closeButton: true });
      }
    );
  }


  /**
   * Filtrar usuarios por nombre, nombre de usuario y correo
   */
  filter() {
    this.usersFilter = this.users.filter((user: { fullname: string, username: string, mail: string }) => {
      let filter = true;
      if (this.search) {
        filter = user.fullname.toLowerCase().includes(this.search.toLowerCase()) ||
          user.username.toLowerCase().includes(this.search.toLowerCase()) ||
          user.mail.toLowerCase().includes(this.search.toLowerCase());
      }
      return filter;
    });
  }

  /**
   * Exportar usuarios en pdf
   */
  exportToPDF() {
    const content: HTMLElement = this.usersTable.nativeElement;

    html2canvas(content, { scale: 3, logging: true }).then(canvas => {
      const imgData = canvas.toDataURL('image/png');
      const pdf = new jsPDF();
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = pdf.internal.pageSize.getHeight();
      pdf.addImage(imgData, 'PNG', 0, 0, 100, 50);
      pdf.save('users.pdf');
    });
  }

}
