import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

import { AppService } from '../../app.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private AppService: AppService, private http: HttpClient) { }

  getProducts() {
    const url = this.AppService.urlBase() + '/products';
    return this.http.get<any>(url);
  }

  getProduct(id: number) {
    const url = this.AppService.urlBase() + `/product/${id}`;
    return this.http.get<any>(url);
  }

  save(data: any) {
    const url = this.AppService.urlBase() + `/product/save`;
    return this.http.post<any>(url, data).pipe(
      catchError(error => {
        if (error.status === 400 || error.status === 404) {
          return throwError(error.error.message);
        } else {
          return throwError(error.status, error.error.message);
        }
      })
    );
  }

  edit(id: number, data: any) {
    const url = this.AppService.urlBase() + `/product/edit/${id}`;
    return this.http.put<any>(url, data).pipe(
      catchError(error => {
        if (error.status === 400 || error.status === 404) {
          return throwError(error.error.message);
        } else {
          return throwError(error.status, error.error.message);
        }
      })
    );
  }

  delete(id: number) {
    const url = this.AppService.urlBase() + `/product/delete/${id}`;
    return this.http.delete<any>(url);
  }

  /**
   * More functions
   */
  statusChange(id: number, data: any) {
    const url = this.AppService.urlBase() + `/product/state_change/${id}`;
    return this.http.put<any>(url, data).pipe(
      catchError(error => {
        if (error.status === 400 || error.status === 404) {
          return throwError(error.error.message);
        } else {
          return throwError(error.status, error.error.message);
        }
      })
    );
  }

  uploadImage(id: number, image: any) {
    const url = this.AppService.urlBase() + `/product/upload_image/${id}`;
    const data = new FormData(image);
    return this.http.put<any>(url, data).pipe(
      catchError(error => {
        if (error.status === 400 || error.status === 404) {
          return throwError(error.error.message);
        } else {
          return throwError(error.status, error.error.message);
        }
      })
    );
  }

  getImage(id: number): Observable<Blob>{
    const url = this.AppService.urlBase() + `/product/image/${id}`;
    return this.http.get(url, { responseType: 'blob' });
  }

}
